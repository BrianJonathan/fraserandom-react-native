import { StatusBar } from "expo-status-bar";
import React, { Component, useEffect, useState, ScrollView } from "react";
import { StyleSheet, Text, View } from "react-native";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { randomText: {} };
  }
  componentDidMount() {
    this.setRandomText();
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>{"Open up App.js to start working on your app!"}</Text>
        <StatusBar style="auto" />
      </View>
    );
  }

  setRandomText = (textRnd) => {
    console.log(textRnd);
    this.setState({ randomText: this.getRandomText() });
  };
  async getRandomText() {
    debugger;
    const url = "https://forismatic.com/en/api/";
    var headers = {}

    let response = await fetch(url, {
      method: "GET",
      mode: "cors",
      headers: headers,
    })
      .then((response) => {
        debugger;
        if (!response.ok) {
          throw new Error(response.error);
        }
        return response.json();
      })
      .then((data) => {
        document.getElementById("messages").value = data.messages;
      })
      .catch(function (error) {
        debugger;
        document.getElementById("messages").value = error;
      });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
